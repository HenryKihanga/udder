import 'package:flutter/material.dart';

class Ride {
  final int id;
  final String arriveTime;
  final String capacity;
  final String fare;

  Ride(
      {@required this.id,
      @required this.arriveTime,
      @required this.capacity,
      @required this.fare});
}

List<Ride> rides = [
  Ride(id: 1, arriveTime: "2 MIN", capacity: "3 PEOPLE", fare: "2.00"),
  Ride(id: 2, arriveTime: "6 MIN", capacity: "4 PEOPLE", fare: "3.00"),
  Ride(id: 3, arriveTime: "7 MIN", capacity: "5 PEOPLE", fare: "3.50"),
  Ride(id: 4, arriveTime: "10 MIN", capacity: "6 PEOPLE", fare: "4.00")
];
