import 'package:flutter/cupertino.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:udder/models/ride.dart';

mixin UdderConnectedModel on Model {
//for select button model
  int _currentIndex;
  bool _isTaxiSelected = false;
  bool _isSuperXSelected = false;
  bool _isBlackSelected = false;
  bool _isSvSelected = false;

//for Ride model

  List<Ride> _availableRides = [];

//RidesModel
  int _rates = 0;
  int _currentRateButtonIndex;
  bool _isFirstStarSelected = false;
  bool _isSecondStarSelected = false;
  bool _isThirdStarSelected = false;
  bool _isFourthStarSelected = false;
  bool _isFifthStarSelected = false;

//UtilityModel
  bool _showContainer = false;

//choice widget
  double _choiceWidgetHeight;

//cars model
  List<Ride> _availableCars;
}

mixin SelectedButtonModel on UdderConnectedModel {
  void setCurrentIndex({@required int buttonId}) {
    _currentIndex = buttonId;

//setting bool value to specific picked button
    buttonId == 0 ? _isTaxiSelected = true : _isTaxiSelected = false;
    buttonId == 1 ? _isSuperXSelected = true : _isSuperXSelected = false;
    buttonId == 2 ? _isBlackSelected = true : _isBlackSelected = false;
    buttonId == 3 ? _isSvSelected = true : _isSvSelected = false;

    notifyListeners();
  }

//getter for Id of current button clicked
  get currentIndex => _currentIndex;
//getters for boolen value of selected button
  get taxiSelected => _isTaxiSelected;
  get superXSelected => _isSuperXSelected;
  get blackSelected => _isBlackSelected;
  get svSelected => _isSvSelected;
}

mixin RideModal on UdderConnectedModel {
  void getRides() {
    _availableRides = rides;

    notifyListeners();
  }

//getting list of Rides
  get availableRides => _availableRides;

//get available rides if no return empty
  // List<Ride> getAvailablerides() {
  //   if (_availableRides == null) {
  //     return <Ride>[];
  //   }
  //   return List<Ride>.from(_availableRides);
  // }

}

mixin RideSelectorModel on UdderConnectedModel {
//getters
  double get choiceWidgetHeight => _choiceWidgetHeight;
  int get currentIndex => _currentIndex;

//setters
  void setChoiceWidgetHeight({@required height}) {
    _choiceWidgetHeight = height;
    notifyListeners();
  }

  void setCurrentIndex({@required index}) {
    _currentIndex = index;
    notifyListeners();
  }
}

mixin RatesModel on UdderConnectedModel {
  void rateButtonIndex({@required int buttonIndex}) {
    _currentRateButtonIndex = buttonIndex;

    if (buttonIndex == 0) {
      _isFirstStarSelected = true;
      _isSecondStarSelected = false;
      _isThirdStarSelected = false;
      _isFourthStarSelected = false;
      _isFifthStarSelected = false;
      _rates = 1;
    } else if (buttonIndex == 1) {
      _isFirstStarSelected = true;
      _isSecondStarSelected = true;
      _isThirdStarSelected = false;
      _isFourthStarSelected = false;
      _isFifthStarSelected = false;
      _rates = 2;
    } else if (buttonIndex == 2) {
      _isFirstStarSelected = true;
      _isSecondStarSelected = true;
      _isThirdStarSelected = true;
      _isFourthStarSelected = false;
      _isFifthStarSelected = false;
      _rates = 3;
    } else if (buttonIndex == 3) {
      _isFirstStarSelected = true;
      _isSecondStarSelected = true;
      _isThirdStarSelected = true;
      _isFourthStarSelected = true;
      _isFifthStarSelected = false;
      _rates = 4;
    } else if (buttonIndex == 4) {
      _isFirstStarSelected = true;
      _isSecondStarSelected = true;
      _isThirdStarSelected = true;
      _isFourthStarSelected = true;
      _isFifthStarSelected = true;
      _rates = 5;
    }
    notifyListeners();
  }

//get index of ratestar button clicked
  get currentRateButtonIndex => _currentRateButtonIndex;

//get bool value of rate star clicked
  get firstStarSelected => _isFirstStarSelected;
  get secondStarSelected => _isSecondStarSelected;
  get thirtStarSelected => _isThirdStarSelected;
  get fourthStarSelected => _isFourthStarSelected;
  get fifthStarSelected => _isFifthStarSelected;

//get rates
  get rates => _rates;
}

mixin UtilityModel on UdderConnectedModel {
//animate container on enter destination field tapped
  void showHiddenContainer({@required bool show}) {
    _showContainer = show;

    notifyListeners();
  }

  bool get showContainer => _showContainer;
}

mixin CarsModel on UdderConnectedModel {
  List<Ride> get availableCars => _availableCars;
  void setavailableCars() {
    _availableCars = rides;
    notifyListeners();
  }
}

mixin GoogleMapModel on UdderConnectedModel {
  Map<MarkerId, Marker> _markers = <MarkerId, Marker>{};
  MarkerId _markerId;
  Marker _marker;

  Map<MarkerId, Marker> _locationDestinationMarkers = <MarkerId, Marker>{};
  MarkerId _destinationMarkerId;
  Marker _destinationMarker;

  MarkerId _departureMarkerId;
  Marker _departureMarker;

  Map<PolylineId, Polyline> _polyLinebtnTwoPoints = <PolylineId, Polyline>{};

  void prepareCurrentLocationMarker() {
    _markerId = MarkerId('73622');

    BitmapDescriptor.fromAssetImage(ImageConfiguration(size: Size(48, 48)),
            'assets/icons/pickup_pin.png')
        .then((value) {
      _marker = Marker(
          icon: value,
          markerId: _markerId,
          draggable: true,
          position: LatLng(-6.7781483, 39.2249217),
          infoWindow: InfoWindow(
              title: "My current Location",
              onTap: () {
                print('marker tapped');
              }));

      _markers[_markerId] = _marker;
      notifyListeners();
    });
  }

  Map<MarkerId, Marker> get markers => _markers;

  void lastTripMarkers() {
    _destinationMarkerId = MarkerId('65627');
    _departureMarkerId = MarkerId('5666');

    BitmapDescriptor.fromAssetImage(ImageConfiguration(size: Size(48, 48)),
            'assets/icons/pickuppin.png')
        .then((value) {
      _departureMarker = Marker(
          icon: value,
          markerId: _departureMarkerId,
          draggable: true,
          position: LatLng(-6.7781483, 39.2249217),
          infoWindow: InfoWindow(
              title: "My current Location",
              onTap: () {
                print('marker tapped');
              }));

      _locationDestinationMarkers[_departureMarkerId] = _departureMarker;
      notifyListeners();
    });

    BitmapDescriptor.fromAssetImage(ImageConfiguration(size: Size(48, 48)),
            'assets/icons/destinationpin.png')
        .then((value) {
      _destinationMarker = Marker(
          icon: value,
          markerId: _destinationMarkerId,
          draggable: true,
          position: LatLng(-6.773679, 39.2415134),
          infoWindow: InfoWindow(
              title: "My current Location",
              onTap: () {
                print('marker tapped');
              }));

      _locationDestinationMarkers[_destinationMarkerId] = _destinationMarker;
      //putting line between two points
      _polyLinebtnTwoPoints[PolylineId('5446')] = Polyline(
          polylineId: PolylineId('5446'),
          color: Color(0xff000000),
          width: 1,
          points: <LatLng>[
            LatLng(-6.7781483, 39.2249217),
            LatLng(-6.773679, 39.2415134)
          ]);
      notifyListeners();
    });
  }
//getters
  Map<MarkerId, Marker> get destinationmarkers => _locationDestinationMarkers;
  Map<PolylineId, Polyline> get polyline => _polyLinebtnTwoPoints;
}
