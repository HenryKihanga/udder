import 'package:scoped_model/scoped_model.dart';
import 'package:udder/data/scoped_model.dart';

class MainModel extends Model
    with
        UdderConnectedModel,
        SelectedButtonModel,
        RideModal,
        RatesModel,
        UtilityModel,
        GoogleMapModel {}
