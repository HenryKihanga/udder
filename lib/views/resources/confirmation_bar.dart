import 'package:flutter/material.dart';

class ConfirmationBar extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;
    return Container(
      height: height / 4,
      width: width,
      color: Colors.transparent,
      child: Stack(
        children: <Widget>[
          Align(
            alignment: Alignment.topCenter,
            child: Container(
              height: height / 7.95,
              width: width * 11 / 12,
              decoration: BoxDecoration(
                  color: Colors.white, borderRadius: BorderRadius.circular(20)),
              child: Column(children: <Widget>[
                Container(
                  height: height / 19,
                  padding: EdgeInsets.only(left: 15, right: 15, top: 15),
                  decoration: BoxDecoration(
                      //color: Colors.black,
                      borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(20),
                          topRight: Radius.circular(20))),
                  child: Row(children: <Widget>[
                    Image.asset("assets/icons/apple_pay_btn_filled.png"),
                    Padding(
                      padding: const EdgeInsets.only(left: 12),
                      child: Text(
                        "APPLE PAY",
                        style: TextStyle(
                            fontSize: 15, fontWeight: FontWeight.w600),
                      ),
                    ),
                    Spacer(),
                    Text(
                      "CHANGE",
                      style: TextStyle(
                          fontSize: 10,
                          color: Colors.grey,
                          fontWeight: FontWeight.w800),
                    )
                  ]),
                ),
                Divider(
                  color: Colors.black26,
                ),
                Container(
                  height: height / 21,
                  decoration: BoxDecoration(
                      // color: Colors.blue,
                      borderRadius: BorderRadius.only(
                          bottomLeft: Radius.circular(20),
                          bottomRight: Radius.circular(20))),
                  child: Row(
                    children: <Widget>[
                      Expanded(
                        child: Container(
                          height: height / 19,
                          decoration: BoxDecoration(
                              //    color: Colors.blue,
                              // border: Border(
                              //   right: BorderSide(
                              //       width: 1, color: Colors.red),
                              // ),
                              borderRadius: BorderRadius.only(
                            bottomLeft: Radius.circular(20),
                          )),
                          child: RaisedButton(
                            shape: RoundedRectangleBorder(borderRadius: BorderRadius.only(bottomLeft: Radius.circular(20))),
                            color: Colors.white,
                            child: Column(
                              children: <Widget>[
                                Image.asset(
                                    "assets/icons/fare_estimate_icon.png"),
                                Padding(
                                  padding: const EdgeInsets.only(top: 3),
                                  child: Text("FARE ESTIMATE",
                                      style: TextStyle(
                                          color: Colors.grey,
                                          fontWeight: FontWeight.w800,
                                          fontSize: 10)),
                                )
                              ],
                            ),
                            onPressed: () {},
                          ),
                        ),
                      ),
                      Expanded(
                        child: Container(
                          height: height / 19,
                          decoration: BoxDecoration(
                              // border: Border(
                              //     right: BorderSide(
                              //         width: 1, color: Colors.red),
                              //   ),
                              borderRadius: BorderRadius.only(
                                  bottomRight: Radius.circular(20))),
                          child: RaisedButton(
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.only(
                                    bottomRight: Radius.circular(20))),
                            color: Colors.white,
                            child: Column(
                              children: <Widget>[
                                Image.asset("assets/icons/promo_code_icon.png"),
                                Padding(
                                  padding: const EdgeInsets.only(top: 3),
                                  child: Text("PROMO CODE",
                                      style: TextStyle(
                                          color: Colors.grey,
                                          fontWeight: FontWeight.w800,
                                          fontSize: 10)),
                                )
                              ],
                            ),
                            onPressed: () {},
                          ),
                        ),
                      )
                    ],
                  ),
                ),
              ]),
            ),
          ),
          Align(
            alignment: Alignment.center,
            child: Padding(
              padding: const EdgeInsets.only(top: 80),
              child: Container(
                padding: EdgeInsets.all(2),
                width: width * 11 / 12,
                height: height / 20,
                decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(30)),
                child: RaisedButton(
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(30)),
                  child: Text(
                    "REQUEST SuperX",
                    style: TextStyle(fontSize: 18, fontWeight: FontWeight.w500),
                  ),
                  color: Colors.blue[300],
                  onPressed: () {},
                ),
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(bottom: 10),
            child: Align(
              alignment: Alignment.bottomCenter,
              child: Text(
                "PICKUP TIME IS APPROXIMATELY 2 MINS",
                style: TextStyle(
                    color: Colors.grey,
                    fontSize: 10,
                    fontWeight: FontWeight.w800),
              ),
            ),
          )
        ],
      ),
    );
  }
}
