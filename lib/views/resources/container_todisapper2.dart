import 'package:flutter/material.dart';

class ContainerToDisapper2 extends StatefulWidget {
  final double height;

  const ContainerToDisapper2({Key key,@required this.height}) : super(key: key);
  @override
  _ContainerToDisapper2State createState() => _ContainerToDisapper2State();
}

class _ContainerToDisapper2State extends State<ContainerToDisapper2> {

  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    return AnimatedContainer(
      duration: Duration(seconds: 10),
      height: height,
      color: Colors.white,
      child: Column(
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.all(20),
            child: Container(
              height: height * 1 / 7,
              decoration: BoxDecoration(
                  color: Colors.black, borderRadius: BorderRadius.circular(10)),
              child: Column(
                children: <Widget>[
                  TextFormField(
                    textAlign: TextAlign.center,
                    decoration: InputDecoration(
                        border: InputBorder.none,
                        hintText: 'Current Location',
                        hintStyle: TextStyle(color: Colors.white),
                        prefixIcon:
                            Image.asset("assets/icons/pickup_location.png")),
                  ),
                  Divider(
                    color: Colors.white,
                  ),
                  TextFormField(
                    textAlign: TextAlign.center,
                    decoration: InputDecoration(
                        border: InputBorder.none,
                        hintText: 'Home',
                        hintStyle: TextStyle(color: Colors.white),
                        prefixIcon: Image.asset(
                            "assets/icons/destination_location.png")),
                  ),
                ],
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(20),
            child: Container(
              height: height * 1 / 7,
              decoration: BoxDecoration(
                  color: Colors.black, borderRadius: BorderRadius.circular(10)),
              child: Column(
                children: <Widget>[
                  TextFormField(
                    textAlign: TextAlign.center,
                    decoration: InputDecoration(
                        border: InputBorder.none,
                        hintText: 'Current Location',
                        hintStyle: TextStyle(color: Colors.white),
                        prefixIcon:
                            Image.asset("assets/icons/pickup_location.png")),
                  ),
                  Divider(
                    color: Colors.white,
                  ),
                  TextFormField(
                    textAlign: TextAlign.center,
                    decoration: InputDecoration(
                        border: InputBorder.none,
                        hintText: 'Home',
                        hintStyle: TextStyle(color: Colors.white),
                        prefixIcon: Image.asset(
                            "assets/icons/destination_location.png")),
                  ),
                ],
              ),
            ),
          ),
        ],
      ), 
    );
  }
}
