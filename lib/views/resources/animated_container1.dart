import 'package:flutter/material.dart';
import 'package:udder/views/resources/container_todisapper1.dart';

class AnimatedContainer1Page extends StatefulWidget {
  @override
  _AnimatedContainer1PageState createState() => _AnimatedContainer1PageState();
}

class _AnimatedContainer1PageState extends State<AnimatedContainer1Page> {
  bool _showContainer = false;

  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    return Padding(
      padding: const EdgeInsets.only(top: 10, left: 20, right: 20),
      child: Container(
        height: 220,
        color: Colors.transparent,
        child: Stack(
          children: <Widget>[
            Align(
              alignment: Alignment.topCenter,
              child: Container(
                height: 600,
                color: Colors.transparent,
                child: Column(
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.only(top: 0),
                      child: Container(
                        height: 60,
                        decoration: BoxDecoration(
                            color: Colors.black,
                            borderRadius: BorderRadius.circular(10)),
                        child: Column(
                          children: <Widget>[
                            Container(
                              
                              height: 30,
                              //color: Colors.red,
                              child: TextField(
                                textAlign: TextAlign.center,
                                decoration: InputDecoration(
                                  
                                    border: InputBorder.none,
                                    hintText: 'Current Location',
                                    hintStyle: TextStyle(color: Colors.white ,fontSize: 12),
                                    prefixIcon: Image.asset(
                                        "assets/icons/pickup_location.png")),
                              ),
                            ),
                            // Divider(
                            //   color: Colors.white30,
                            // ),
                            Container(
                              height: 30,
                              //color: Colors.red,
                              child: TextField(
                                onTap: () {
                                  _showContainer = true;
                                },
                                textAlign: TextAlign.center,
                                decoration: InputDecoration(
                                    border: InputBorder.none,
                                    hintText: 'Enter destination',
                                    hintStyle:
                                        TextStyle(color: Colors.grey[600] , fontSize: 12),
                                    prefixIcon: Image.asset(
                                        "assets/icons/destination_location.png")),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                    _showContainer
                        ? ContainerToDisapper1()
                        : Container(
                            // color: Colors.deepPurple,
                            height: 1,
                          ),
                  ],
                ),
              ),
            ),

            // Align(
            //   alignment: Alignment.bottomCenter,
            //   child: RaisedButton(
            //     child: Text("ANIMATE"),
            //     color: Colors.blue,
            //     onPressed: () {
            //       setState(() {
            //         if (_showContainer == false) {
            //           _showContainer = true;
            //         } else {
            //           _showContainer = false;
            //         }
            //       });
            //     },
            //   ),
            // )
          ],
        ),
      ),
    );
  }
}
