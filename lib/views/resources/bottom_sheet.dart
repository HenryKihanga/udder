import 'package:flutter/material.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:udder/constants/constants.dart';
import 'package:udder/data/main_model.dart';
import 'package:udder/views/resources/ride_selector_bar.dart';

class MyBottomSheet extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;
    return ScopedModelDescendant(
      builder: (BuildContext context, Widget child, MainModel model) {
        return Container(
          height: height / 3,
          child: Stack(
            children: <Widget>[
              Align(
                alignment: Alignment.bottomCenter,
                child: Container(
                  height: height / 3.3,
                  decoration: BoxDecoration(
                      color: Colors.grey[300],
                      borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(15),
                          topRight: Radius.circular(15))),
                ),
              ),
              Align(
                alignment: Alignment.topCenter,
                child: RideSelectorBar(
                  onBarTap: () {
                    print("require logic to change borrom shi contents");
                  },
                ),
              ),
              Align(
                alignment: Alignment.center,
                child: Padding(
                  padding: const EdgeInsets.only(top: 50, left: 20, right: 20),
                  //return a card
                  child: Card(
                    child: Container(
                      height: height / 4.8,
                      child: Column(
                        children: <Widget>[
                          Container(
                            height: height / 17,
                            child: Row(
                              children: <Widget>[
                                Expanded(
                                  child: Container(
                                    margin: EdgeInsets.only(
                                        left: 20, right: 20, top: 10),
                                    child: Column(
                                      children: <Widget>[
                                        Text(
                                          "ETA",
                                          style: TextStyle(
                                              color: Colors.grey[900],
                                              fontWeight: FontWeight.w300,
                                              fontSize: 11),
                                        ),
                                        Text(
                                            model
                                                .availableRides[
                                                    model.currentIndex]
                                                .arriveTime,
                                            style: TextStyle(
                                                color: Colors.black,
                                                fontWeight: FontWeight.w500,
                                                fontSize: 13))
                                      ],
                                    ),
                                  ),
                                ),
                                Expanded(
                                  child: Container(
                                    margin: EdgeInsets.only(
                                        left: 20, right: 20, top: 10),
                                    child: Column(
                                      children: <Widget>[
                                        Text(
                                          "MAX SIZE",
                                          style: TextStyle(
                                              color: Colors.grey[900],
                                              fontWeight: FontWeight.w300,
                                              fontSize: 11),
                                        ),
                                        Text(
                                            model
                                                .availableRides[
                                                    model.currentIndex]
                                                .capacity,
                                            style: TextStyle(
                                                color: Colors.black,
                                                fontWeight: FontWeight.w500,
                                                fontSize: 13))
                                      ],
                                    ),
                                  ),
                                )
                              ],
                            ),
                          ),
                          Divider(
                            color: Colors.black,
                          ),
                          Container(
                            height: height / 17,
                            margin: EdgeInsets.only(top: 0),
                            child: Column(
                              children: <Widget>[
                                Text(
                                  "MIN FARE",
                                  style: TextStyle(
                                      color: Colors.grey[900],
                                      fontWeight: FontWeight.w300,
                                      fontSize: 11),
                                ),
                                Text(
                                    model.availableRides[model.currentIndex]
                                        .fare,
                                    style: TextStyle(
                                        color: Colors.black,
                                        fontWeight: FontWeight.w500,
                                        fontSize: 13))
                              ],
                            ),
                          ),
                          Divider(
                            color: Colors.black26,
                          ),
                          
                             Container(
                               width: width,
                              height: height / 26,
                              child: RaisedButton(
                                color: Colors.white,
                                child: Text("GATE FARE ESTIMATE",
                                    style: TextStyle(
                                        color: Colors.black,
                                        fontWeight: FontWeight.w500,
                                        fontSize: 13)),
                                onPressed: () {
                                  Navigator.pop(context);
                              Navigator.pushNamed(context, confirmationPage);
                                },
                              ),
                            ),
                            
                        ],
                      ),
                    ),
                  ),
                ),
              )
            ],
          ),
        );
      },
    );
  }
}

//  child: Card(
//               child: Container(
//                 height: 170,
//                 // color: Colors.red,
//                 child: Column(
//                   children: <Widget>[
//                     Container(
//                       height: 50,
//                       child: Row(
//                         children: <Widget>[
//                           Expanded(
//                             child: Container(
//                               margin:
//                                   EdgeInsets.only(left: 20, right: 20, top: 10),
//                               child: Column(
//                                 children: <Widget>[
//                                   Text(
//                                     "ETA",
//                                     style: TextStyle(
//                                         color: Colors.grey[900],
//                                         fontWeight: FontWeight.w300,
//                                         fontSize: 12),
//                                   ),
//                                   Text(time,
//                                       style: TextStyle(
//                                           color: Colors.black,
//                                           fontWeight: FontWeight.w500,
//                                           fontSize: 15))
//                                 ],
//                               ),
//                             ),
//                           ),
//                           Expanded(
//                             child: Container(
//                               margin:
//                                   EdgeInsets.only(left: 20, right: 20, top: 10),
//                               child: Column(
//                                 children: <Widget>[
//                                   Text(
//                                     "MAX SIZE",
//                                     style: TextStyle(
//                                         color: Colors.grey[900],
//                                         fontWeight: FontWeight.w300,
//                                         fontSize: 12),
//                                   ),
//                                   Text(capasity,
//                                       style: TextStyle(
//                                           color: Colors.black,
//                                           fontWeight: FontWeight.w500,
//                                           fontSize: 15))
//                                 ],
//                               ),
//                             ),
//                           )
//                         ],
//                       ),
//                     ),
//                     Divider(
//                       color: Colors.black26,
//                     ),
//                     Container(
//                       height: 40,
//                       margin: EdgeInsets.only(top: 10),
//                       child: Column(
//                         children: <Widget>[
//                           Text(
//                             "MIN FARE",
//                             style: TextStyle(
//                                 color: Colors.grey[900],
//                                 fontWeight: FontWeight.w300,
//                                 fontSize: 12),
//                           ),
//                           Text(fare,
//                               style: TextStyle(
//                                   color: Colors.black,
//                                   fontWeight: FontWeight.w500,
//                                   fontSize: 15))
//                         ],
//                       ),
//                     ),
//                     Divider(
//                       color: Colors.black26,
//                     ),
//                     Container(
//                       height: 20,
//                       child: Text("GATE FARE ESTIMATE",
//                           style: TextStyle(
//                               color: Colors.black,
//                               fontWeight: FontWeight.w500,
//                               fontSize: 15)),
//                     )
//                   ],
//                 ),
//               ),
//             ),
