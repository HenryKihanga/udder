import 'package:flutter/material.dart';

class AnimatedContainer2Page extends StatefulWidget {
  @override
  _AnimatedContainer2PageState createState() => _AnimatedContainer2PageState();
}

class _AnimatedContainer2PageState extends State<AnimatedContainer2Page> {
  double _height1 = 200;
  double _height2 = 0;
  bool _showContainer = false;

  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    return Scaffold(
      backgroundColor: Colors.green,
      body: Padding(
        padding: const EdgeInsets.only(top: 100),
        child: Container(
          height: 650,
          color: Colors.red,
          child: Stack(
            children: <Widget>[
              Align(
                alignment: Alignment.topCenter,
                child: AnimatedContainer(
                  duration: Duration(seconds: 5),
                  height: _height1,
                  color: Colors.white24,
                  child: Column(
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.all(20),
                        child: Container(
                          height: height * 1 / 7,
                          decoration: BoxDecoration(
                              color: Colors.black,
                              borderRadius: BorderRadius.circular(10)),
                          child: Column(
                            children: <Widget>[
                              TextFormField(
                                textAlign: TextAlign.center,
                                decoration: InputDecoration(
                                    border: InputBorder.none,
                                    hintText: 'Current Location',
                                    hintStyle: TextStyle(color: Colors.white),
                                    prefixIcon: Image.asset(
                                        "assets/icons/pickup_location.png")),
                              ),
                              Divider(
                                color: Colors.white,
                              ),
                              TextFormField(
                                textAlign: TextAlign.center,
                                decoration: InputDecoration(
                                    border: InputBorder.none,
                                    hintText: 'Home',
                                    hintStyle: TextStyle(color: Colors.white),
                                    prefixIcon: Image.asset(
                                        "assets/icons/destination_location.png")),
                              ),
                            ],
                          ),
                        ),
                      ),
                      _showContainer
                          ? AnimatedContainer(
      duration: Duration(seconds: 30),
      height: _height2,
      color: Colors.white,
      child: Column(
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.all(20),
            child: Container(
              height: height * 1 / 7,
              decoration: BoxDecoration(
                  color: Colors.black, borderRadius: BorderRadius.circular(10)),
              child: Column(
                children: <Widget>[
                  TextFormField(
                    textAlign: TextAlign.center,
                    decoration: InputDecoration(
                        border: InputBorder.none,
                        hintText: 'Current Location',
                        hintStyle: TextStyle(color: Colors.white),
                        prefixIcon:
                            Image.asset("assets/icons/pickup_location.png")),
                  ),
                  Divider(
                    color: Colors.white,
                  ),
                  TextFormField(
                    textAlign: TextAlign.center,
                    decoration: InputDecoration(
                        border: InputBorder.none,
                        hintText: 'Home',
                        hintStyle: TextStyle(color: Colors.white),
                        prefixIcon: Image.asset(
                            "assets/icons/destination_location.png")),
                  ),
                ],
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(20),
            child: Container(
              height: height * 1 / 7,
              decoration: BoxDecoration(
                  color: Colors.black, borderRadius: BorderRadius.circular(10)),
              child: Column(
                children: <Widget>[
                  TextFormField(
                    textAlign: TextAlign.center,
                    decoration: InputDecoration(
                        border: InputBorder.none,
                        hintText: 'Current Location',
                        hintStyle: TextStyle(color: Colors.white),
                        prefixIcon:
                            Image.asset("assets/icons/pickup_location.png")),
                  ),
                  Divider(
                    color: Colors.white,
                  ),
                  TextFormField(
                    textAlign: TextAlign.center,
                    decoration: InputDecoration(
                        border: InputBorder.none,
                        hintText: 'Home',
                        hintStyle: TextStyle(color: Colors.white),
                        prefixIcon: Image.asset(
                            "assets/icons/destination_location.png")),
                  ),
                ],
              ),
            ),
          ),
        ],
      ), 
    )
                          : Container(
                              height: 10,
                            )
                    ],
                  ),
                ),
              ),
              Align(
                alignment: Alignment.bottomCenter,
                child: RaisedButton(
                  child: Text("ANIMATE"),
                  color: Colors.blue,
                  onPressed: () {
                    setState(() {
                      if (_height1 == 200 && _height2==0) {
                        _showContainer = true;
                        _height1 = 600;
                        _height2 =350;
                      } else {
                        _showContainer = false;
                        _height1 = 200;
                        _height2 =0;
                      }
                    });
                  },
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
