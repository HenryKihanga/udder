import 'package:flutter/material.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:udder/data/main_model.dart';

typedef RideSelectorBarOnTap = Function();

class RideSelectorBar extends StatelessWidget {
  final RideSelectorBarOnTap onBarTap;

  const RideSelectorBar({Key key, @required this.onBarTap}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    //double height = MediaQuery.of(context).size.height;
    return ScopedModelDescendant(
      builder: (BuildContext context, Widget child, MainModel model) {
        return Container(
          padding: EdgeInsets.all(1.5),
          height: 45,
          width: width * 3 / 4,
          child: Row(
            children: <Widget>[
              Expanded(
                child: InkWell(
                  onTap: () { if(model.taxiSelected == true)
                    Navigator.pop(context);
                    model.setCurrentIndex(buttonId: 0);
                    model.getRides();
                   
                    onBarTap();
                  },
                  child: Container(
                    height: 45,
                    child: Center(
                      child: Text("TAXI",
                          style: TextStyle(color: Colors.black, fontSize: 11)),
                    ),
                    decoration: BoxDecoration(
                        color: model.taxiSelected
                            ? Colors.blue[300]
                            : Colors.white,
                        borderRadius: BorderRadius.circular(50)),
                  ),
                ),
              ),
              Expanded(
                child: InkWell(
                  onTap: () {if(model.superXSelected == true)
                    Navigator.pop(context);
                    model.setCurrentIndex(buttonId: 1);
                    model.getRides();
                    
                    onBarTap();
                  },
                  child: Container(
                    height: 45,
                    child: Center(
                      child: Text(
                        "SuperX",
                        style: TextStyle(color: Colors.black, fontSize: 11),
                      ),
                    ),
                    decoration: BoxDecoration(
                        color: model.superXSelected
                            ? Colors.blue[300]
                            : Colors.white,
                        borderRadius: BorderRadius.circular(50)),
                  ),
                ),
              ),
              Expanded(
                child: InkWell(
                  onTap: () {
                     if(model.blackSelected == true)
                    Navigator.pop(context);
                    model.setCurrentIndex(buttonId: 2);
                    model.getRides();
                   
                    onBarTap();
                  },
                  child: Container(
                    height: 45,
                    child: Center(
                      child: Text("BLACK",
                          style: TextStyle(color: Colors.black, fontSize: 11)),
                    ),
                    decoration: BoxDecoration(
                        color: model.blackSelected
                            ? Colors.blue[300]
                            : Colors.white,
                        borderRadius: BorderRadius.circular(50)),
                  ),
                ),
              ),
              Expanded(
                child: InkWell(
                  onTap: () {
                    if(model.svSelected == true)
                    Navigator.pop(context);
                    model.setCurrentIndex(buttonId: 3);
                    model.getRides();
                    
                    onBarTap();
                  },
                  child: Container(
                    height: 45,
                    child: Center(
                      child: Text("SUV",
                          style: TextStyle(color: Colors.black, fontSize: 11)),
                    ),
                    decoration: BoxDecoration(
                        color:
                            model.svSelected ? Colors.blue[300] : Colors.white,
                        borderRadius: BorderRadius.circular(50)),
                  ),
                ),
              ),
            ],
          ),
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(50), color: Colors.white),
        );
      },
    );
  }
}
