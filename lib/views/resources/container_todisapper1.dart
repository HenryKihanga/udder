import 'package:flutter/material.dart';

class ContainerToDisapper1 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    return Container(
      height: 160,
      color: Colors.transparent,
      child: Column(
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.only(top: 5),
            child: Container(
              height: 60,
              decoration: BoxDecoration(
                  color: Colors.black, borderRadius: BorderRadius.circular(10)),
              child: Column(
                children: <Widget>[
                  Container(
                      height: 30,
                      padding: EdgeInsets.only(left: 10),
                      decoration: BoxDecoration(
                          //  color: Colors.red,
                          borderRadius: BorderRadius.only(
                              topLeft: Radius.circular(10),
                              topRight: Radius.circular(10))),
                      child: Row(
                        children: <Widget>[
                          Image.asset("assets/icons/home_icon.png"),
                          Padding(
                            padding: const EdgeInsets.only(left: 10),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text(
                                  "Home",
                                  style: TextStyle(color: Colors.white , fontSize: 12),
                                ),
                                Text(
                                  "1235 McGilchrist Blvd",
                                  style: TextStyle(color: Colors.grey,fontSize: 12),
                                ),
                              ],
                            ),
                          )
                        ],
                      )),
                  // Divider(
                  //   color: Colors.white,
                  // ),
                  Container(
                      height: 30,
                      padding: EdgeInsets.only(left: 10),
                      decoration: BoxDecoration(
                          //  color: Colors.red,
                          borderRadius: BorderRadius.only(
                              bottomLeft: Radius.circular(10),
                              bottomRight: Radius.circular(10))),
                      child: Row(
                        children: <Widget>[
                          Image.asset("assets/icons/work_icon.png"),
                          Padding(
                            padding: const EdgeInsets.only(left: 10),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text(
                                  "Work",
                                  style: TextStyle(color: Colors.white ,fontSize: 12),
                                ),
                                Text(
                                  "1 Mason St",
                                  style: TextStyle(color: Colors.grey ,fontSize: 12),
                                ),
                              ],
                            ),
                          )
                        ],
                      )),
                ],
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 5),
            child: Container(
              height: 90,
              decoration: BoxDecoration(
                  color: Colors.black, borderRadius: BorderRadius.circular(10)),
              child: Column(
                children: <Widget>[
                  Container(
                     height: 30,
                      padding: EdgeInsets.only(left: 10),
                      decoration: BoxDecoration(
                          //  color: Colors.red,
                          borderRadius: BorderRadius.only(
                              topLeft: Radius.circular(10),
                              topRight: Radius.circular(10))),
                      child: Row(
                        children: <Widget>[
                          Image.asset("assets/icons/history_icon.png"),
                          Padding(
                            padding: const EdgeInsets.only(left: 10),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text(
                                  "Viverri Coffe",
                                  style: TextStyle(color: Colors.white ,fontSize: 12),
                                ),
                                Text(
                                  "JL Gandaria 1 N0 63, Daerah khusus Ibukota Jakarta",
                                  style: TextStyle(color: Colors.grey,fontSize: 12),
                                ),
                              ],
                            ),
                          )
                        ],
                      )),
                  // Divider(
                  //   color: Colors.white,
                  // ),
                  Container(
                      height: 30,
                      padding: EdgeInsets.only(left: 10),
                      //color: Colors.blue,

                      child: Row(
                        children: <Widget>[
                          Image.asset("assets/icons/history_icon.png"),
                          Padding(
                            padding: const EdgeInsets.only(left: 10),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text(
                                  "One Fitteen Coffee",
                                  style: TextStyle(color: Colors.white,fontSize: 12),
                                ),
                                Text(
                                  "JL Pluit Sakti Raya N0 117,Pluit,Penjaringan,Daerah",
                                  style: TextStyle(color: Colors.grey,fontSize: 12),
                                ),
                              ],
                            ),
                          )
                        ],
                      )),
                  // Divider(
                  //   color: Colors.white,
                  // ),
                  Container(
                      height: 30,
                      padding: EdgeInsets.only(left: 10),
                      decoration: BoxDecoration(
                          //  color: Colors.red,
                          borderRadius: BorderRadius.only(
                              bottomLeft: Radius.circular(10),
                              bottomRight: Radius.circular(10))),
                      child: Row(
                        children: <Widget>[
                          Image.asset("assets/icons/history_icon.png"),
                          Padding(
                            padding: const EdgeInsets.only(left: 10),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text(
                                  "Ombea Kofie",
                                  style: TextStyle(color: Colors.white,fontSize: 12),
                                ),
                                Text(
                                  "JL Pluit Baru N0 117,Pluit,Penjaringan,Daerah",
                                  style: TextStyle(color: Colors.grey,fontSize: 12),
                                ),
                              ],
                            ),
                          )
                        ],
                      )),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
