import 'package:flutter/material.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:udder/constants/constants.dart';
//import 'package:udder/constants/constants.dart';
import 'package:udder/data/main_model.dart';
//import 'package:udder/views/pages/finished_review_dialog.dart';
import 'package:udder/views/resources/animated_container1.dart';
import 'package:udder/views/resources/bottom_sheet.dart';
//import 'package:udder/views/resources/container_todisapper1.dart';
import 'package:udder/views/resources/map_page.dart';
import 'package:udder/views/resources/ride_selector_bar.dart';

class HomePage extends StatefulWidget {
  final MainModel model;

  const HomePage({Key key, @required this.model}) : super(key: key);
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  void initState() {
    widget.model.getRides();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;

    return ScopedModelDescendant(
      builder: (BuildContext context, Widget child, MainModel model) {
        return Scaffold(
            backgroundColor: Colors.grey[300],
            appBar: AppBar(
              centerTitle: true,
              elevation: 0,
              backgroundColor: Colors.black,
              leading: IconButton(
                icon: Image.asset("assets/icons/menuslider.png"),
                onPressed: () {
                  Navigator.pushNamed(context, finishedReviewDialog);
                },
              ),
              title: Text(
                "UDDER",
                style: TextStyle(
                    color: Colors.white,
                    fontSize: 20,
                    fontWeight: FontWeight.bold),
              ),
            ),
            body: Stack(
              children: <Widget>[
                Card(
                  elevation: 2,
                  child: Container(
                    //contains the map
                    child: MapPage(markerSet: model.markers
                    ,),
                    height: height * 6 / 7,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.only(
                            bottomLeft: Radius.circular(10),
                            bottomRight: Radius.circular(10))),
                  ),
                ),
                AnimatedContainer1Page(),
                   
                Align(
                    alignment: Alignment.bottomCenter,
                    child: Padding(
                        padding: const EdgeInsets.all(10),

                        //call the car selector
                        child: RideSelectorBar(
                          onBarTap: () {
                            _showCarDetails();
                          },
                        ))),
                // Align(
                //   alignment: Alignment.center,
                //   child: Container(
                //     height: 200,
                //     child: Column(
                //       children: <Widget>[
                //         RaisedButton(
                //           child: Text("ANIMATED CONTAINER1"),
                //           onPressed: () {
                //             Navigator.pushNamed(
                //                 context, animatedContainer1Page);
                //           },
                //         ),
                //         RaisedButton(
                //           child: Text("ANIMATED CONTAINER2"),
                //           onPressed: () {
                //             Navigator.pushNamed(
                //                 context, animatedContainer2Page);
                //           },
                //         ),
                //         RaisedButton(
                //           child: Text("FINISHED REVIEWDIALOG"),
                //           onPressed: () {
                //             showDialog(
                //                 context: context,
                //                 barrierDismissible: true,
                //                 builder: (BuildContext context) {
                //                   return FinishedReviewDialog();
                //                 });
                //             //Navigator.pushNamed(context, finishedReviewPopup);
                //           },
                //         ),
                //       ],
                //     ),
                //   ),
                // )
              ],
            ));
      },
    );
  }

  void _showCarDetails() {
    showModalBottomSheet(
        backgroundColor: Colors.transparent,
        builder: (BuildContext context) {
          return MyBottomSheet();
        },
        context: context);
  }
}
