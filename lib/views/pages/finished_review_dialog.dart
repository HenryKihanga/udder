import 'package:flutter/material.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:udder/data/main_model.dart';
import 'package:udder/views/resources/map_page.dart';

class FinishedReviewDialog extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;
    return ScopedModelDescendant(
      builder: (BuildContext context, Widget child, MainModel model) {
        return Scaffold(
            backgroundColor: Colors.white30,
            body: Padding(
              padding: const EdgeInsets.only(
                top: 0,
                bottom: 0,
              ),
              child: Center(
                child: Container(
                  
                  height: height*5/6  ,
                    width: width * 19 / 21,
                    decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(20)),
                    child: Column(
                      children: <Widget>[
                        Container(
                          padding: EdgeInsets.all(10),
                          height: height / 12,
                          decoration: BoxDecoration(
                              // color: Colors.green,
                              borderRadius: BorderRadius.only(
                                  topLeft: Radius.circular(20),
                                  topRight: Radius.circular(20))),
                          child: Row(
                            children: <Widget>[
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Text(
                                    "YOUR LAST TRIP",
                                    style: TextStyle(
                                        fontSize: 14,
                                        fontWeight: FontWeight.w700),
                                  ),
                                  Text(
                                    "Sunday 11:31 AM",
                                    style: TextStyle(
                                        color: Colors.grey, fontSize: 10),
                                  )
                                ],
                              ),
                              Spacer(),
                              Padding(
                                padding: const EdgeInsets.only(left: 0),
                                child: Container(
                                  //color: Colors.red,
                                  height: height/30,
                                  width: 100,
                                  child: RaisedButton(
                                    color: Colors.white,
                                    child: Center(
                                      child: Text(
                                        "NEED HELP?",
                                        style: TextStyle(
                                            fontSize: 10,
                                            fontWeight: FontWeight.w700,
                                            color: Colors.grey[600]),
                                      ),
                                    ),
                                    onPressed: () {},
                                  ),
                                ),
                              )
                            ],
                          ),
                        ),
                        Container(
                          height: height / 4,
                         // color: Colors.blue,
                          child: Stack(
                            children: <Widget>[
                              MapPage(markerSet: model.destinationmarkers,polylineSet: model.polyline,),
                              Align(
                                alignment: Alignment.bottomCenter,
                                child: Container(
                                  padding: EdgeInsets.only(
                                      left: 75, right: 17, top: 8),
                                  height: height / 18,
                                  color: Colors.grey[300],
                                  child: Row(
                                    children: <Widget>[
                                      Padding(
                                        padding: const EdgeInsets.only(
                                            left: 0, top: 0, bottom: 0),
                                        child: Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: <Widget>[
                                            Text(
                                              "Benedict",
                                              style: TextStyle(
                                                  fontSize: 12,
                                                  fontWeight: FontWeight.w600),
                                            ),
                                            Text(
                                              "SuperX",
                                              style: TextStyle(
                                                  color: Colors.grey,
                                                  fontSize: 10),
                                            )
                                          ],
                                        ),
                                      ),
                                      Spacer(),
                                      Padding(
                                        padding: const EdgeInsets.only(left: 0),
                                        child: Text(
                                          "USD 4.76",
                                          style: TextStyle(
                                              fontSize: 16,
                                              fontWeight: FontWeight.w700),
                                        ),
                                      )
                                    ],
                                  ),
                                ),
                              ),
                              Align(
                                alignment: Alignment.bottomLeft,
                                child: Padding(
                                  padding: const EdgeInsets.all(8),
                                  child: CircleAvatar(
                                    backgroundImage:
                                        AssetImage("assets/images/user.png"),
                                    radius: 30,
                                  ),
                                ),
                              )
                            ],
                          ),
                        ),
                        Container(
                          height: height * 1.5 / 3,
                          width: width * 19 / 21,
                          decoration: BoxDecoration(
                                //color: Colors.red,
                              borderRadius: BorderRadius.only(
                                  bottomLeft: Radius.circular(20),
                                  bottomRight: Radius.circular(20))),
                          child: Stack(
                            children: <Widget>[
                              Align(
                                alignment: Alignment.center,
                                child: Container(
                                  height: height / 10,
                                  //color: Colors.red,
                                  width: width * 2 / 3,
                                  child: Column(
                                    children: <Widget>[
                                      Text(
                                        "Rate Your Ride",
                                        style: TextStyle(color: Colors.grey),
                                      ),
                                      Container(
                                        // color: Colors.blue,
                                        height: height / 15,
                                        width: width * 2 / 3,
                                        child: Row(
                                          children: <Widget>[
                                            Expanded(
                                                child: InkWell(
                                              onTap: () {
                                                model.rateButtonIndex(
                                                    buttonIndex: 0);
                                              },
                                              child: Image.asset(model
                                                      .firstStarSelected
                                                  ? "assets/icons/star_icon_filled.png"
                                                  : "assets/icons/star_icon.png"),
                                            )),
                                            Expanded(
                                                child: InkWell(
                                              onTap: () {
                                                model.rateButtonIndex(
                                                    buttonIndex: 1);
                                              },
                                              child: Image.asset(model
                                                      .secondStarSelected
                                                  ? "assets/icons/star_icon_filled.png"
                                                  : "assets/icons/star_icon.png"),
                                            )),
                                            Expanded(
                                                child: InkWell(
                                              onTap: () {
                                                model.rateButtonIndex(
                                                    buttonIndex: 2);
                                              },
                                              child: Image.asset(model
                                                      .thirtStarSelected
                                                  ? "assets/icons/star_icon_filled.png"
                                                  : "assets/icons/star_icon.png"),
                                            )),
                                            Expanded(
                                                child: InkWell(
                                              onTap: () {
                                                model.rateButtonIndex(
                                                    buttonIndex: 3);
                                              },
                                              child: Image.asset(model
                                                      .fourthStarSelected
                                                  ? "assets/icons/star_icon_filled.png"
                                                  : "assets/icons/star_icon.png"),
                                            )),
                                            Expanded(
                                                child: InkWell(
                                              onTap: () {
                                                model.rateButtonIndex(
                                                    buttonIndex: 4);
                                              },
                                              child: Image.asset(model
                                                      .fifthStarSelected
                                                  ? "assets/icons/star_icon_filled.png"
                                                  : "assets/icons/star_icon.png"),
                                            )),
                                          ],
                                        ),
                                      )
                                    ],
                                  ),
                                ),
                              ),
                              model.firstStarSelected
                                  ? Align(
                                      alignment: Alignment.bottomCenter,
                                      child: Container(
                                        height: height / 20,
                                        width: width * 19 / 21,
                                        decoration: BoxDecoration(
                                            color: Colors.blue[300],
                                            borderRadius: BorderRadius.only(
                                                bottomLeft: Radius.circular(20),
                                                bottomRight:
                                                    Radius.circular(20))),
                                        child: RaisedButton(
                                          shape: RoundedRectangleBorder(
                                              borderRadius: BorderRadius.only(
                                                  bottomLeft:
                                                      Radius.circular(20),
                                                  bottomRight:
                                                      Radius.circular(20))),
                                          child: Text(
                                            "SUBMIT",
                                            style: TextStyle(
                                                color: Colors.white,
                                                fontSize: 15),
                                          ),
                                          color: Colors.blue[300],
                                          onPressed: () {
                                            print("Rated as " +
                                                model.rates.toString());
                                          },
                                        ),
                                      ),
                                    )
                                  : Container()
                            ],
                          ),
                        ),
                      ],
                    )),
              ),
            ));
      },
    );
  }
}
