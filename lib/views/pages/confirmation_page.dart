import 'package:flutter/material.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:udder/data/main_model.dart';
import 'package:udder/views/resources/confirmation_bar.dart';
import 'package:udder/views/resources/map_page.dart';

class ConfirmationPage extends StatelessWidget {
  
  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    //  double width = MediaQuery.of(context).size.width;
    return ScopedModelDescendant(builder: (BuildContext context, Widget child, MainModel model) {

      return Scaffold(
        backgroundColor: Colors.grey[300],
        appBar: AppBar(
          centerTitle: true,
          elevation: 0,
          backgroundColor: Colors.black,
          leading: IconButton(
            icon: Image.asset("assets/icons/back_btn.png"),
            onPressed: () {
              Navigator.pop(context);
            },
          ),
          title: Text(
            "Confirmation",
            style: TextStyle(
              color: Colors.white,
              fontSize: 18,
            ),
          ),
        ),
        body: Stack(
          children: <Widget>[
            Card(
              elevation: 2,
              child: Container(
                child: MapPage(markerSet: model.destinationmarkers,),
                height: height * 5 / 7.08,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.only(
                        bottomLeft: Radius.circular(10),
                        bottomRight: Radius.circular(10))),
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(20),
              child: Container(
                height: height * 1 / 7,
                decoration: BoxDecoration(
                    color: Colors.black,
                    borderRadius: BorderRadius.circular(10)),
                child: Column(
                  children: <Widget>[
                    TextFormField(
                      textAlign: TextAlign.center,
                      decoration: InputDecoration(
                          border: InputBorder.none,
                          hintText: 'Current Location',
                          hintStyle: TextStyle(color: Colors.white ,fontSize: 12),
                          prefixIcon:
                              Image.asset("assets/icons/pickup_location.png")),
                    ),
                    // Divider(
                    //   color: Colors.white,
                    // ),
                    TextFormField(
                      textAlign: TextAlign.center,
                      decoration: InputDecoration(
                          border: InputBorder.none,
                          hintText: 'Home',
                          hintStyle: TextStyle(color: Colors.white ,fontSize: 12),
                          prefixIcon: Image.asset(
                              "assets/icons/destination_location.png")),
                    ),
                  ],
                ),
              ),
            ),
            Align(
                alignment: Alignment.bottomCenter,
                //calls navigation bar
                child: ConfirmationBar()),
          ],
        ));
    },);
  }
}
