import 'package:flutter/material.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:udder/constants/constants.dart';
import 'package:udder/data/main_model.dart';
import 'package:udder/views/pages/confirmation_page.dart';
import 'package:udder/views/pages/finished_review_dialog.dart';
import 'package:udder/views/pages/home_page.dart';
import 'package:udder/views/resources/animated_container1.dart';
import 'package:udder/views/resources/animated_container2.dart';

class App extends StatefulWidget {
  @override
  _AppState createState() => _AppState();
}

class _AppState extends State<App> {
  final MainModel _model = MainModel();


  @override
  void initState() {
    _model.prepareCurrentLocationMarker();
    _model.lastTripMarkers();
    super.initState();
  }


  @override
  Widget build(BuildContext context) {
return ScopedModel(model: _model, child: MaterialApp(
      title: 'UDDER',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
   
        primarySwatch: Colors.blue,
      ),
      home: HomePage(model: _model,),

      routes: {
        confirmationPage: (BuildContext context) => ConfirmationPage(),
        animatedContainer1Page:(BuildContext context) => AnimatedContainer1Page(),
        animatedContainer2Page:(BuildContext context) => AnimatedContainer2Page(),
        finishedReviewDialog : (BuildContext context) =>FinishedReviewDialog(),
      },
    ),);

  }
}